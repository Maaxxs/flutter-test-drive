import 'package:flutter/material.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  static const String _title = 'FF Albstadt Ebingen';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: MyHomePage(title: _title),
    );
  }
}

class ButtonClicked extends StatefulWidget {
  ButtonClicked({Key key, @required this.counter}) : super(key: key);

  final int counter;

  @override
  State<StatefulWidget> createState() => _ButtonClickedState(counter: counter);

}

class _ButtonClickedState extends State<ButtonClicked> {
  int counter;

  _ButtonClickedState({Key key, this.counter});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Plus Icon has been clicked'),
            Text(
              '$counter',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            Text('times!')
          ],
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _selectedIndex = 0;

  static const TextStyle optionStyle = TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  void _incrementCounter() {
    setState(() {
      _counter++;
      print('Counter ist now $_counter');
    });
  }


  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
        Text(
          'Plus Icon was tapped\n$_counter\ntimes.',
//        style: optionStyle,
          textAlign: TextAlign.center,
      ),
      ButtonClicked(counter: _counter),
      Text(
        'Index 0: Home',
        style: optionStyle,
      ),
      const Text(
        'Index 1: Business',
        style: optionStyle,
      ),
      const Text(
        'Index 2: School',
        style: optionStyle,
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            title: Text('Business'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            title: Text('School'),
          ),
        ],
        currentIndex: _selectedIndex,
//        selectedItemColor: Colors.amber[800],
        selectedItemColor: Colors.lightGreen,
        onTap: _onItemTapped,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
